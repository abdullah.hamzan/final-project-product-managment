package com.productDataManagment.productDataManagment.controller;


import com.productDataManagment.productDataManagment.entity.Product;
import com.productDataManagment.productDataManagment.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/product/v1")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;
    @CrossOrigin
    @GetMapping("/get")
    public ResponseEntity<List<Product>> getAllData(){
        List<Product> result = productRepository.findAll();
        if (result == null || result.size() == 0 || result.isEmpty()){
            return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/get/{id}")
    public ResponseEntity<Product> getOneData(@PathVariable(value = "id") Long id){

        try {
            Product result = productRepository.findById(id).orElse(null);

            if (result == null  || result.equals(null)){

                return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(result,HttpStatus.OK);
        }catch (Exception e){
            log.error("error:",e);

            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

    @CrossOrigin
    @PutMapping("/update/{id}")
    public ResponseEntity<String> updateOneData(@PathVariable(value = "id") Long id,@RequestBody Product product){
        try {
          Optional<Product> result = productRepository.findById(id);
          result.ifPresent(res->{
              res.setName(product.getName());
              res.setPrice(product.getPrice());
              productRepository.save(res);
          });
            return new ResponseEntity<>("succes do update data",HttpStatus.OK);


        }catch (Exception e){
            log.error("error:",e);

            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }



    @CrossOrigin
    @PostMapping("/add")
    public ResponseEntity<Product> postData(@RequestBody Product product){
        try{
            Product p= productRepository.save(product);
            log.info("product:",p);
            return new ResponseEntity<>(p,HttpStatus.CREATED);

        }catch (Exception e){
            log.error("e:",e);
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @CrossOrigin
    @DeleteMapping
    public ResponseEntity<String> deleteData(@RequestParam String productId){

        try{
            productRepository.deleteById(Long.valueOf(productId));
            log.info("productId",productId);

            return new ResponseEntity<>("succes delete data",HttpStatus.OK);


        }catch (Exception e){

            log.error("error:",e.getMessage());
            return  new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);


        }


    }




}
