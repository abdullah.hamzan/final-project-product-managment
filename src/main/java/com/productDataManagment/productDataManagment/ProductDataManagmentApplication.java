package com.productDataManagment.productDataManagment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductDataManagmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductDataManagmentApplication.class, args);
	}

}
