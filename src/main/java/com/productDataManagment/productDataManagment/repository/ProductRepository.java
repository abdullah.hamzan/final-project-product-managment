package com.productDataManagment.productDataManagment.repository;

import com.productDataManagment.productDataManagment.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product,Long> {
}
