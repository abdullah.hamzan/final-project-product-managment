package com.productDataManagment.productDataManagment.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="tb_product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Long price;
}
